# exercicesjs

Exercices d’algorithmie basique en javascript.

Chaque exercice est indépendant, sous forme d'un test unitaire jest.  
Le code doit être implémenté dans la fonction de travail (celle contenant le commentaire `insert code here`).

Pour exécuter le code faire `npm test <nom du test>` (le nom du test est écrit dans le header de chaque fichier).