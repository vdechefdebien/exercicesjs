// Given a phrase, reverse the order of the characters of each word.
// run tests with : "npm test reversewords"

function reverseWords(input) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("Reverse Words", () => {
    it("Should reverse words", () => {
        expect(reverseWords("I love JavaScript!")).toBe("I evol !tpircSavaJ")
    })
})
