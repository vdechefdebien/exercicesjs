// Create a square matrix of a given size in which elements are in spiral order.
// run tests with : "npm test MatrixSpiral"

function spiral(number) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("Matrix Spiral", () => {
    it("Should implement matrix spiral", () => {
        expect(spiral(3)).toBe([[1, 2, 3], [8, 9, 4], [7, 6, 5]])
    })
})
