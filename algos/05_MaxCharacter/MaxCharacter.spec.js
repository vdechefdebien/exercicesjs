// Given a string of characters, return the character that appears the most often.
// run tests with : "npm test maxcharacter"

function max(input) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("Max Character", () => {
    it("Should return max character", () => {
        expect(max("Hello World!")).toEqual("l")
    })
})
