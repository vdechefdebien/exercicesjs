// Given an integer, reverse the order of the digits.
// run tests with : "npm test integerreversal"

function reverse(input) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("Integer Reversal", () => {
    it("Should reverse integer", () => {
        expect(reverse(1234)).toEqual(4321)
        expect(reverse(-1200)).toEqual(-21)
    })
})
