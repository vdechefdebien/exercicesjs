// Implement a performant recursive function for the fibonacci series.
// run tests with : "npm test MemoizedFibonacci"

function fibonacci(input) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("Memoized Fibonacci", () => {
    it("Should implement memoized fibonacci", () => {
        expect(fibonacci(6)).toBe(8)
        expect(fibonacci(10)).toBe(55)
    })
})
